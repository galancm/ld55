use std::time::Duration;

use bevy::{prelude::*, sprite::MaterialMesh2dBundle};
use bevy_xpbd_2d::{
    math::*, prelude::*, PhysicsSchedule, PhysicsStepSet, SubstepSchedule, SubstepSet,
};
use std::cmp::min;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: "Demon Fighter".into(),
                name: Some("demon_fighter.app".into()),
                resolution: (1024., 576.).into(),
                present_mode: bevy::window::PresentMode::AutoVsync,
                prevent_default_event_handling: true,
                enabled_buttons: bevy::window::EnabledButtons {
                    maximize: false,
                    ..Default::default()
                },
                resizable: false,
                ..default()
            }),
            ..default()
        }))
        .add_plugins((PhysicsPlugins::default(), PhysicsDebugPlugin::default()))
        .add_systems(Startup, setup)
        .add_systems(Update, bevy::window::close_on_esc)
        .add_systems(Update, sync_depth)
        .add_systems(Update, handle_punch_input)
        .add_systems(Update, handle_punch_queue)
        .add_systems(Update, update_punch_timers)
        .add_systems(
            PhysicsSchedule,
            move_player.before(PhysicsStepSet::BroadPhase),
        )
        .add_systems(
            // Run collision handling in substep schedule
            SubstepSchedule,
            kinematic_collision.in_set(SubstepSet::SolveUserConstraints),
        )
        .add_systems(PostProcessCollisions, emit_damage)
        .add_systems(Update, take_damage)
        .add_event::<DamageEvent>()
        .run();
}

#[derive(Component)]
struct Player;

#[derive(Component, Default)]
struct Damage {
    power: i32,
    active: bool,
}

#[derive(Component)]
struct Health {
    max: i32,
    current: i32,
}

#[derive(Default)]
enum PunchSide {
    #[default]
    Left,
    Right,
}

#[derive(Component, Default)]
struct Punch {
    side: PunchSide,
    active_timer: Timer,
    cooldown_timer: Timer,
    queued: bool,
}

#[derive(Component)]
struct Demon;

#[derive(PhysicsLayer, Clone, Copy, Debug)]
enum GameLayer {
    Bounds,
    Player,
    Demon,
}

#[derive(Event)]
struct DamageEvent {
    damage: i32,
    target: Entity,
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    // Camera
    commands.spawn(Camera2dBundle::default());

    // Player
    commands
        .spawn((
            Player,
            MaterialMesh2dBundle {
                mesh: meshes.add(Rectangle::new(32., 64.)).into(),
                material: materials.add(Color::ALICE_BLUE),
                transform: Transform::from_xyz(0., 0., 1.),
                ..default()
            },
            RigidBody::Kinematic,
            Collider::compound(vec![(
                Vec2::new(0., -24.),
                0.0,
                Collider::rectangle(32., 16.),
            )]),
            CollisionLayers::new(GameLayer::Player, [GameLayer::Bounds, GameLayer::Demon]),
        ))
        .with_children(|parent| {
            parent.spawn((
                Punch {
                    side: PunchSide::Left,
                    ..Default::default()
                },
                Damage {
                    power: 1,
                    ..default()
                },
                Sensor,
                Collider::circle(10.),
                CollisionLayers::new(GameLayer::Player, [GameLayer::Demon]),
                MaterialMesh2dBundle {
                    mesh: meshes.add(Circle::new(10.)).into(),
                    material: materials.add(Color::RED),
                    visibility: Visibility::Hidden,
                    transform: Transform::from_xyz(35., 20., 0.),
                    ..default()
                },
            ));
        })
        .with_children(|parent| {
            parent.spawn((
                Punch {
                    side: PunchSide::Right,
                    ..Default::default()
                },
                Damage {
                    power: 1,
                    ..default()
                },
                Sensor,
                Collider::circle(10.),
                CollisionLayers::new(GameLayer::Player, [GameLayer::Demon]),
                MaterialMesh2dBundle {
                    mesh: meshes.add(Circle::new(10.)).into(),
                    material: materials.add(Color::RED),
                    visibility: Visibility::Hidden,
                    transform: Transform::from_xyz(35., 0., 0.),
                    ..default()
                },
            ));
        });

    // Walls

    // top
    commands.spawn((
        MaterialMesh2dBundle {
            mesh: meshes.add(Rectangle::new(1024., 192.)).into(),
            material: materials.add(Color::PURPLE),
            transform: Transform::from_xyz(0., 192., 0.),
            ..default()
        },
        RigidBody::Static,
        Collider::rectangle(1024., 192.),
        CollisionLayers::new(GameLayer::Bounds, [GameLayer::Player, GameLayer::Demon]),
    ));
    // bottom
    commands.spawn((
        RigidBody::Static,
        Collider::rectangle(1024., 20.),
        TransformBundle::from_transform(Transform::from_xyz(0., -278., 0.)),
        CollisionLayers::new(GameLayer::Bounds, [GameLayer::Player, GameLayer::Demon]),
    ));
    // left
    commands.spawn((
        RigidBody::Static,
        Collider::rectangle(20., 576.),
        TransformBundle::from_transform(Transform::from_xyz(-502., 0., 0.)),
        CollisionLayers::new(GameLayer::Bounds, [GameLayer::Player, GameLayer::Demon]),
    ));
    //right
    commands.spawn((
        Health { max: 3, current: 3 },
        RigidBody::Static,
        Collider::rectangle(256., 576.),
        TransformBundle::from_transform(Transform::from_xyz(384., 0., 0.)),
        CollisionLayers::new(GameLayer::Demon, [GameLayer::Player, GameLayer::Bounds]),
    ));

    // // DEMON!!
    // commands.spawn(
    //     RigidBody::KinematicPositionBased,
    //     Collider::rectangle(64,64),
    //     SpatialBundle {
    //         transform: Transform.from
    //     }
}

fn move_player(
    keys: Res<ButtonInput<KeyCode>>,
    mut player_controllers: Query<&mut LinearVelocity, With<Player>>,
) {
    let mut motion = Vec3::ZERO;

    const MOVE_SPEED: f32 = 100.;

    if keys.pressed(KeyCode::ArrowRight) {
        motion.x += 1.;
    }
    if keys.pressed(KeyCode::ArrowLeft) {
        motion.x -= 1.;
    }
    if keys.pressed(KeyCode::ArrowUp) {
        motion.y += 1.;
    }
    if keys.pressed(KeyCode::ArrowDown) {
        motion.y -= 1.;
    }

    motion = motion.normalize_or_zero() * MOVE_SPEED;
    motion.x *= 1.5;

    for mut linear_velocity in player_controllers.iter_mut() {
        linear_velocity.x = motion.x;
        linear_velocity.y = motion.y;
    }
}

fn sync_depth(mut transforms: Query<&mut GlobalTransform>) {
    for mut global_transform in &transforms {
        global_transform.translation().z = global_transform.translation().y - 200.
    }
}

const PUNCH_QUEUE_BUFFER_TIME: Duration = Duration::from_millis(100);

fn handle_punch_input(
    keys: Res<ButtonInput<KeyCode>>,
    mut punches: Query<(&mut Punch, &mut Damage, &mut Visibility)>,
) {
    // skip punches entirely if we're blocked by an attack
    let mut shortest_active = Duration::from_secs(60);
    for (punch, _damage, _visiblity) in &mut punches {
        if punch.active_timer.remaining() > PUNCH_QUEUE_BUFFER_TIME {
            return;
        } else {
            shortest_active = min(punch.active_timer.remaining(), shortest_active);
        }
    }

    if keys.just_pressed(KeyCode::KeyZ) {
        for (mut punch, mut damage, mut visiblity) in &mut punches {
            if matches!(punch.side, PunchSide::Left) {
                if punch.cooldown_timer.remaining().is_zero() {
                    if shortest_active.is_zero() {
                        punch.active_timer =
                            Timer::new(Duration::from_millis(250), TimerMode::Once);
                        damage.active = true;
                        *visiblity = Visibility::Visible;
                        punch.queued = false;
                    } else {
                        punch.queued = true;
                    }
                } else if punch.cooldown_timer.remaining() <= PUNCH_QUEUE_BUFFER_TIME {
                    punch.queued = true;
                }
            }
        }
    }
    if keys.just_pressed(KeyCode::KeyX) {
        for (mut punch, mut damage, mut visiblity) in &mut punches {
            if matches!(punch.side, PunchSide::Right) {
                if punch.cooldown_timer.remaining().is_zero() {
                    if shortest_active.is_zero() {
                        punch.active_timer =
                            Timer::new(Duration::from_millis(250), TimerMode::Once);
                        damage.active = true;
                        *visiblity = Visibility::Visible;
                        punch.queued = false;
                    } else {
                        punch.queued = true;
                    }
                } else if punch.cooldown_timer.remaining() <= PUNCH_QUEUE_BUFFER_TIME {
                    punch.queued = true;
                }
            }
        }
    }
}

fn handle_punch_queue(mut punches: Query<(&mut Punch, &mut Damage, &mut Visibility)>) {
    for (punch, _damage, _visiblity) in &punches {
        if punch.active_timer.remaining() > PUNCH_QUEUE_BUFFER_TIME {
            return;
        }
    }

    for (mut punch, mut damage, mut visiblity) in &mut punches {
        if punch.queued
            && (punch.active_timer.just_finished() || punch.cooldown_timer.just_finished())
        {
            punch.active_timer = Timer::new(Duration::from_millis(250), TimerMode::Once);
            damage.active = true;
            *visiblity = Visibility::Visible;
            punch.queued = false;
        }
    }
}

fn update_punch_timers(
    mut punches: Query<(&mut Punch, &mut Damage, &mut Visibility)>,
    time: Res<Time>,
) {
    for (mut punch, mut damage, mut visibility) in &mut punches {
        punch.active_timer.tick(time.delta());
        punch.cooldown_timer.tick(time.delta());

        if punch.active_timer.just_finished() {
            damage.active = false;
            punch.cooldown_timer = Timer::new(Duration::from_millis(700), TimerMode::Once);
            *visibility = Visibility::Hidden;
        }
    }
}

fn emit_damage(
    mut events: EventWriter<DamageEvent>,
    mut query: Query<(&mut Damage, &CollidingEntities)>,
) {
    for (mut damage, colliding_entities) in &mut query {
        if damage.active {
            for target in colliding_entities.iter() {
                events.send(DamageEvent {
                    damage: damage.power,
                    target: *target,
                });
            }

            damage.active = false;
        }
    }
}

fn take_damage(
    mut events: EventReader<DamageEvent>,
    mut commands: Commands,
    mut demon_query: Query<(Entity, &mut Health)>,
) {
    for &DamageEvent { damage, target } in events.read() {
        for (entity, mut health) in &mut demon_query {
            health.current -= damage;

            if health.current <= 0 {
                commands.entity(entity).despawn()
            }
        }
    }
}

// fn handle_punch_collisions(mut collision_event_reader: EventReader<Collision>, mut punches: Query<Punch, Sensor>, colliders: Query<Collider>

//     for punch, sensor in punches.iter(){
//         if punch.timer.is_some() {

//         }
//     }
// }

// fn print_collisions(mut collision_event_reader: EventReader<Collision>) {
//     for Collision(contacts) in collision_event_reader.read() {
//         println!(
//             "Entities {:?} and {:?} are colliding",
//             contacts.entity1, contacts.entity2,
//         );
//     }
// }

fn kinematic_collision(
    collisions: Res<Collisions>,
    mut bodies: Query<(&RigidBody, &mut Position, &Rotation)>,
) {
    // Iterate through collisions and move the kinematic body to resolve penetration
    for contacts in collisions.iter() {
        // If the collision didn't happen during this substep, skip the collision
        if !contacts.during_current_substep {
            continue;
        }
        if let Ok([(rb1, mut position1, rotation1), (rb2, mut position2, _)]) =
            bodies.get_many_mut([contacts.entity1, contacts.entity2])
        {
            for manifold in contacts.manifolds.iter() {
                for contact in manifold.contacts.iter() {
                    if contact.penetration <= Scalar::EPSILON {
                        continue;
                    }
                    if rb1.is_kinematic() && !rb2.is_kinematic() {
                        position1.0 -= contact.global_normal1(rotation1) * contact.penetration;
                    } else if rb2.is_kinematic() && !rb1.is_kinematic() {
                        position2.0 += contact.global_normal1(rotation1) * contact.penetration;
                    }
                }
            }
        }
    }
}
